#!flask/bin/python

from services.video_processor import create_app, db
from services.video_processor.models import User, Notification, Task

app = create_app()
app.run(host='0.0.0.0',port=5001,debug=True)

