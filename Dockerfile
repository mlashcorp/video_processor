FROM ubuntu:18.04

RUN useradd -ms /bin/bash video_processor

WORKDIR /home/video_processor

COPY requirements.txt requirements.txt
RUN apt-get update && apt-get install -y redis-server ffmpeg python python-pip
RUN pip install virtualenv
RUN virtualenv flask
RUN flask/bin/pip --no-cache-dir install -r requirements.txt
RUN flask/bin/pip install gunicorn

COPY services services
RUN mkdir uploads
RUN mkdir processed
COPY migrations migrations
COPY run.py boot.sh video_silence_service.py ./

ENV FLASK_APP run.py

RUN chown -R video_processor:video_processor ./
USER video_processor

EXPOSE 5000

ENTRYPOINT ["./boot.sh"]