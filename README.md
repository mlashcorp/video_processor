# Video processing tool 

## Instalation

First, build the docker container:

```
docker build -t silence_removal_service:latest .
```

Next, run the container:

```
docker run --name silence_removal_service3 -d -p 8000:5000 --rm silence_removal_service:latest
```

In this example, we are rerouting port 5000 to port 8000. This can be changed to another port, but 
you must always use port 5000 for the container.

Finally, access localhost:8000 on your web browser.

## How to use

The container comes with no users registred, so the first step must be to create a user to access the tool,
by going to:

```
localhost:8000/registration
```

To access the tool:

```
localhost:8000
```

After logging in, start by dragging a video file to the drop zone. The next screen allows the user to change
2 settings related with the tool: minimum silence length, and noise threshold.

* minimum silence length indicates for at least how long a silence must last to be considered (default 1 second)
* noise threshold controls how low the volume must be to be considered "silence". The values are proportional to the mean square 
of the sound samples

Pressing analyse will present feedback to the user on how those settings will affect the output video. 

Pressing start processing will re-encode the video with only the portions that weren't cut by the noise removal algorithm