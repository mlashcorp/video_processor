#!/bin/bash
source flask/bin/activate
flask db upgrade
#redis-server &
#rq worker video-tasks &
exec gunicorn -b :5000 --access-logfile - --error-logfile - video_silence_service:app
