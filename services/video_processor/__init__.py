from flask import Flask
from flask_wtf.csrf import CSRFProtect
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from flask_login import LoginManager
from redis import Redis
import rq

csrf = CSRFProtect()
db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'routes.login'

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    migrate.init_app(app, db)

    from services.video_processor.routes import bp as routes_bp
    app.register_blueprint(routes_bp)

    login.init_app(app)
    csrf.init_app(app)
    app.redis = Redis.from_url(app.config['REDIS_URL'])
    #change rq worker timeout to enable long running tasks (we are re-encoding the video...)
    app.task_queue = rq.Queue('video-tasks', connection=app.redis,default_timeout=3600)
    app.config["UPLOAD_FOLDER"] = "uploads"
    app.config["PROCESSED_FOLDER"] = "processed"
    app.config["IMG_FOLDER"] = "services/video_processor/static/img"
    
    return app

from services.video_processor import models

