from services.video_processor import db, login
from datetime import datetime
from wtforms import DateField, Form, TextField,StringField,BooleanField, SelectMultipleField, validators
from wtforms.fields.core import SelectField
from wtforms.validators import DataRequired
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
import redis
import rq
from time import time
from flask import current_app
import json

class Config(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    silence_length = db.Column(db.Integer, default=1)
    noise_threshold = db.Column(db.Float, default=0.0001)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    uploads = db.relationship('UploadedVideo', backref='author', lazy='dynamic')
    processed_videos = db.relationship('ProcessedVideo', backref='author', lazy='dynamic')
    
    tasks = db.relationship('Task', backref='user', lazy='dynamic')
    notifications = db.relationship('Notification', backref='user',lazy='dynamic')

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), user=self)
        db.session.add(n)
        return n

    def launch_task(self, name, description, *args, **kwargs):
        rq_job = current_app.task_queue.enqueue('services.video_processor.tasks.' + name, self.id,
                                                *args, **kwargs)
        task = Task(id=rq_job.get_id(), name=name, description=description,
                    user=self)
        db.session.add(task)
        return task

    def get_tasks_in_progress(self):
        return Task.query.filter_by(user=self, complete=False).all()

    def get_task_in_progress(self, name):
        return Task.query.filter_by(name=name, user=self,
                                    complete=False).first()

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)   
    def __repr__(self):
        return '<User {}>'.format(self.username)


class Task(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(128), index=True)
    description = db.Column(db.String(128))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    complete = db.Column(db.Boolean, default=False)

    def get_rq_job(self):
        try:
            rq_job = rq.job.Job.fetch(self.id, connection=current_app.redis)
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None
        return rq_job

    def get_progress(self):
        job = self.get_rq_job()
        return job.meta.get('progress', 0) if job is not None else 100
    
    def get_message(self):
        job = self.get_rq_job()
        return job.meta.get('message', 0) if job is not None else ""
    

class UploadedVideo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(256), unique=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    analysed = db.Column(db.Boolean, default=False)
    complete = db.Column(db.Boolean, default=False)
    silence_length = db.Column(db.Integer, default=1)
    noise_threshold = db.Column(db.Float, default=0.0001)

    #pre-processing data
    payload_json = db.Column(db.Text)

    timestamp = db.Column(db.Float, index=True, default=time)
    def get_data(self):
        return json.loads(str(self.payload_json))
    def __repr__(self):
        return '<File {}>'.format(self.filename)


class ProcessedVideo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(256), unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    #pre-processing data
    payload_json = db.Column(db.Text)

    def get_data(self):
        return json.loads(str(self.payload_json))
        
    def __repr__(self):
        return '<File {}>'.format(self.filename)


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.Float, index=True, default=time)
    payload_json = db.Column(db.Text)

    def get_data(self):
        return json.loads(str(self.payload_json))


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

    