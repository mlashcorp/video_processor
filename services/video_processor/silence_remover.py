#!/usr/bin/env python

"""
This file contains the main back-end functionality - removing silent portions from videos

All methods that write to disk must receive the user specific path prefix
"""

from scipy.io import wavfile
import os
import numpy as np
import argparse
import pexpect, subprocess, re
import time

#Default filenames for intermediate jobs
CONCAT_FILE = "concat.txt"
OUTPUT_AUDIO_FILENAME = "output-audio.wav"
OUTPUT_VIDEO_FILENAME = "final_cut.mp4"


def windows(signal, window_size, step_size):
    """
    Generator function to implement a sliding window of 
    window size with a stride of step_size

    Using generators will allow for smaller memory footprints
    """
    if type(window_size) is not int:
        raise AttributeError("Window size must be an integer.")
    if type(step_size) is not int:
        raise AttributeError("Step size must be an integer.")
    for i_start in xrange(0, len(signal), step_size):
        i_end = i_start + window_size
        if i_end >= len(signal):
            break
        yield signal[i_start:i_end]


def energy(samples):
    """
    Energy or power metric for audio

    TODO: Study alternatives
    """
    return np.sum(np.power(samples, 2.)) / float(len(samples))


def get_silence_gaps(data):
    previous_value = 0
    index = 0
    is_noisy = False
    start_sound = -1
    for x in data:
        #If we were in a silence window, and now the data is noisy
        if not is_noisy and not x:
            is_noisy = True
            start_sound = index

        #If the data is currently silence
        if (x and not previous_value and index > 0):
            is_noisy = False
            yield (start_sound,index)
        previous_value = x
        index += 1
    if is_noisy:
        yield (start_sound,index-1)


def process_audio(min_silence_length,silence_threshold,work_path_prefix=""):
    """This method will analyse the audio waveform and pin point rising and falling transitions
    in the silence/noisy states
    
    returns a start and stop times of noisy portions of the video
    """
    start_time = time.time()
    audio_path = os.path.join(work_path_prefix,OUTPUT_AUDIO_FILENAME)
    rate, data  = wavfile.read(audio_path, mmap=True)
    max_amplitude = np.iinfo(data.dtype).max
    max_energy = energy([max_amplitude])

    #setup variables
    step_duration = 1.0
    window_size = int(step_duration * rate)
    step_size = int(step_duration * rate)
    #generate windows
    signal_windows = windows(data,window_size,step_size)
    #generate energy per window
    window_energy = (energy(w) / max_energy for w in signal_windows)
    energy_array = list(window_energy)
    print("silence threshold is: "+str(silence_threshold))
    perc = np.percentile(energy_array,int(silence_threshold))
    #generate filtered data labeling as true sections with silence
    window_silence = (e > perc for e in energy_array)
    cut_times = (r for r in get_silence_gaps(window_silence))
    x_data = np.arange(len(data))/float(rate)

    #convert cuts to seconds and filter by min silence length
    slices = []
    for cuts in cut_times:
        start = cuts[0] * step_duration
        stop = cuts[1] * step_duration
        #if (stop-start) >= min_silence_length:
        slices.append((start,stop))
    end_time = time.time()
    print("processing audio took "+str(end_time-start_time)+" seconds")
    return slices, step_duration, len(data)/rate,(x_data,data)


def extract_audio(input_video_path,work_path_prefix=""):
    audio_path = os.path.join(work_path_prefix,OUTPUT_AUDIO_FILENAME)
    os.system("rm "+str(audio_path))
    os.system("ffmpeg -i "+str(input_video_path)+" "+str(audio_path))


def get_video_framerate(input_file):
    try:
        result = subprocess.Popen('ffmpeg -i '+str(input_file), shell=True, stderr=subprocess.PIPE).communicate()[1]
        fps_string = re.findall('\d+\.\d+ fps', result)[0]
        fps = float(fps_string.split(' ')[0])
        print "framerate is: "+str(fps)
        return fps
    except:
        #if something went wrong, go with a sane default
        return 30.0


def process_video(input_video_path,cut_times,output_file_path=OUTPUT_VIDEO_FILENAME,set_progress=None,get_status=None,work_path_prefix=""):
    video_fps = get_video_framerate(input_video_path)
    ffmpeg_cmd_options = []
    video_names = []
    video_idx = 0

    audio_path = os.path.join(work_path_prefix,OUTPUT_AUDIO_FILENAME)
    concat_path = os.path.join(work_path_prefix,CONCAT_FILE)
    #just in case, remove possible video files in local storage
    os.system("rm -rf "+work_path_prefix+"/*.mp4")
    
    #estimate total time for progress bar feedback and create ffmpeg commands to process each video segment
    total_time = 0
    for cuts in cut_times:
        total_time += ((cuts[1]-cuts[0]))
        #not using -copy - full re-encode of video! (slow)
        video_name = str(video_idx)+".mp4"
        ffmpeg_cmd_options.append("-ss "+str(cuts[0])+" -t "+str((cuts[1]-cuts[0]))+" "+work_path_prefix+"/"+video_name)
        video_names.append(video_name)
        video_idx += 1
    
    estimated_number_of_frames = total_time*video_fps
    print "estimated total time: "+str(total_time)
    print "estimated_number of frames: "+str(estimated_number_of_frames)
    
    frame_count = 0
    #process each segment by calling ffmpeg with prebuilt command options, and look at output to estimate progress
    for cmd_options in ffmpeg_cmd_options:
        last_frame = 0
        print "cmd -> "+str("ffmpeg -i "+str(input_video_path)+" "+str(cmd_options))
        thread = pexpect.spawn("ffmpeg -i "+str(input_video_path)+" "+str(cmd_options))
        cpl = thread.compile_pattern_list([
        pexpect.EOF,
        "frame= *\d+",
        '(.+)'
        ])
        running_status = True
        while running_status:
            running_status = get_status()
            i = thread.expect_list(cpl, timeout=None)
            if i == 0: # EOF
                print "the sub process exited"
                break
            elif i == 1:
                frame_number_string = thread.match.group(0)
                print "-> "+str(frame_number_string)
                #TODO: error handling regex
                
                #generate ongoing progress from multiple encodes
                frame_number = int(re.findall('\d+', frame_number_string)[0])
                frame_count += frame_number - last_frame
                last_frame = frame_number

                set_progress(30+((frame_count/estimated_number_of_frames)*65.0),"processing frame "+str(frame_count))
                thread.close
            elif i == 2:
                pass

        #If we exited due to a cancel request ...
        if not running_status:
            print "Stopping background worker early ..."
            status = thread.terminate(force=True)
            print "killed process with status: "+str(status)
            return
    
    #write concat.txt file that will be used to concat segments
    with open(concat_path,'w') as wfp:
        for video in video_names:
            wfp.write("file '"+str(video)+"'\n")
        wfp.flush()

    print "working dir: "+str(os.getcwd())
    print "running cmd -> "+str("ffmpeg -f concat -i "+str(concat_path)+" -c copy "+str(output_file_path))
    #concat parts
    os.system("ffmpeg -f concat -i "+str(concat_path)+" -c copy "+str(output_file_path))
    


def get_silence_metrics(input_file,min_silence_length,silence_threshold,work_path_prefix=""):
    """
    Auxiliary method for pre-processing video

    Will extract audio and look for silence cuts only
    """
    extract_audio(input_file,work_path_prefix)
    cut_times,step_duration, total_time, (x_data,y_data) = process_audio(min_silence_length,silence_threshold,work_path_prefix)

    return cut_times,total_time,x_data,y_data