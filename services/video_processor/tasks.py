from services.video_processor import create_app
from services.video_processor import db
from services.video_processor.models import User, UploadedVideo,Task,ProcessedVideo
from rq import get_current_job
import silence_remover
import os,sys, traceback
import json
import time
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

app = create_app()
app.app_context().push()

def _get_job_status():
    job = get_current_job()
    if job:
        job.refresh()
        if  job.get_status() == "stop":
            return False
        else:
            return True



def _set_task_progress(progress,message="",channel="task_progress"):
    """
    This method will use the add_notification method of the user schema
    to communicate with the user's client javascript polling for progress
    of tasks
    """
    job = get_current_job()
    if job:
        job.meta['progress'] = progress
        job.meta['message'] = message
        job.save_meta()
        task = Task.query.get(job.get_id())
        task.user.add_notification(channel, {'task_id': job.get_id(),
                                                     'progress': progress,
                                                     'message': message})
        if progress >= 100 or progress < 0:
            task.complete = True
        db.session.commit()

def draw_waveform(cut_times,x_data,y_data,username):
    plt.style.use("dark_background")
    plt.figure(figsize=(10, 4))
    plt.fill_between(x_data, y_data[:,0], y_data[:,1], color='white')
    for cut_start,cut_end in cut_times:
        plt.axvspan(cut_start, cut_end, alpha=0.5, color='green')
    plt.xlim(x_data[0], x_data[-1])

    #remove previous image(s)
    user_path = os.path.join(app.config["IMG_FOLDER"],username)
    os.system("rm -rf "+user_path+"/*.png")
    plt.savefig(os.path.join(user_path, 'waveform.png'),transparent=True)

def analyse_video(user_id):
    """
    Preliminary task of just finding the silence cut times - much faster than full workflow since
    no video needs to be re-encoded
    """
    try:
        user = User.query.get(user_id)
        active_upload = None
        active_upload = UploadedVideo.query.filter(UploadedVideo.complete == False and UploadedVideo.user_id == user_id).first()
    except:
        print "Exception in user code:"
        print '-'*60
        traceback.print_exc(file=sys.stdout)
        print '-'*60

    if active_upload != None:
        try:
            current_upload = active_upload
            user_path = os.path.join(app.config["UPLOAD_FOLDER"],user.username)
            #get input file path
            input_file = os.path.join(user_path,current_upload.filename)
            _set_task_progress(10,"loading data ...",channel = "task_analyse")
            try:
                cut_times,total_time,x_data,y_data = silence_remover.get_silence_metrics(input_file,int(current_upload.silence_length),float(current_upload.noise_threshold),work_path_prefix=user.username)
            except:
                current_upload.complete = True
                _set_task_progress(0,"error encoding video!",channel = "task_analyse")
                #a little hackish, but give 2 seconds before exiting to main dashboard
                time.sleep(2)
                _set_task_progress(-1,"error encoding video!",channel = "task_analyse")
                return
            cuts = [x for x in cut_times]
            sound_segments = invert_slices(cuts,x_data[-1])
            new_time = 0
            for cut in sound_segments:
                new_time += cut[1]-cut[0]
            start_time = time.time()
            #subsample axis
            x_data = np.array(x_data)[1::100]
            y_data = np.array(y_data)[1::100]
            print ("len x_data: "+str(len(x_data)))
            draw_waveform(sound_segments,x_data,y_data,user.username)
            end_time = time.time()
            print("drawing waveform took "+str(end_time-start_time)+" seconds")
            current_upload.payload_json = json.dumps({'cut_times': sound_segments,'total_time':total_time,'new_time':int(new_time)})
            current_upload.analysed = True
            db.session.commit()
            _set_task_progress(100,"done",channel = "task_analyse")

        except:
            print "Exception in user code:"
            print '-'*60
            traceback.print_exc(file=sys.stdout)
            print '-'*60

def invert_slices(slices,end_time):
    inverted_slices = []
    previous_slice_end = 0
    for (start,stop) in slices:
        if start > previous_slice_end:
            inverted_slices.append((previous_slice_end,start))
            previous_slice_end = stop
        else:
            previous_slice_end = stop
    if previous_slice_end < end_time:
        inverted_slices.append((previous_slice_end,end_time))
    return inverted_slices

def process_video(user_id):
    """
    This is the method that will implement the process video task.

    I have chosen to re-encode the videos (instead of using ffmpeg's -copy option)
    to ensure that the output is correct.
    """
    try:
        try:
            user = User.query.get(user_id)
            _set_task_progress(0)
            active_upload = None
            active_upload = UploadedVideo.query.filter(UploadedVideo.complete == False and UploadedVideo.user_id == user_id).first()
            print "active upload is: "+str(active_upload)
        except:
            print "Exception in user code:"
            print '-'*60
            traceback.print_exc(file=sys.stdout)
            print '-'*60
        if active_upload != None:
            try:
                current_upload = active_upload
                user_path = os.path.join(app.config["UPLOAD_FOLDER"],user.username)
                #get input file path
                input_file = os.path.join(user_path,current_upload.filename)
                print "input file path is: "+str(input_file)
                _set_task_progress(10,"extracting audio from video")
                silence_remover.extract_audio(input_file,work_path_prefix=user.username)
                _set_task_progress(15,"looking for silences")
                try:
                    cut_times,step_duration,total_time,(x_data,y_data) = silence_remover.process_audio(int(current_upload.silence_length),float(current_upload.noise_threshold),user.username)
                except:
                    current_upload.complete = True
                    _set_task_progress(0,"error encoding video!")
                    #a little hackish, but give 2 seconds before exiting to main dashboard
                    time.sleep(2)
                    _set_task_progress(-1,"error encoding video!")
                    return
                _set_task_progress(30,"encoding video ... this will take a while")
                #create unique filename for processed videos
                processed_filename = str(current_upload.id)+"_"+current_upload.filename
                #invert cut times to get portions with sound
                sound_segments = invert_slices(cut_times,x_data[-1])
                user_path = os.path.join(app.config["PROCESSED_FOLDER"],user.username)
                silence_remover.process_video(input_file,sound_segments,output_file_path=\
                os.path.join(user_path,processed_filename),set_progress=_set_task_progress,get_status=_get_job_status,work_path_prefix=user.username)
                if _get_job_status():
                    _set_task_progress(98,"almost done")
                    #add processed video
                    pv = ProcessedVideo(filename=processed_filename,user_id=user.id)
                    db.session.add(pv)

                    #set upload as processed
                    current_upload.complete = True
                    _set_task_progress(100,"all done")
                    #commit processed videos to database
                    db.session.commit()
            except:
                print "Exception in user code:"
                print '-'*60
                traceback.print_exc(file=sys.stdout)
                print '-'*60

    except:
        pass
