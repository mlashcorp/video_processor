from flask import request, abort, redirect, url_for, render_template, make_response
from services.video_processor.routes import bp
from services.video_processor import csrf, db
from datetime import datetime
from werkzeug.utils import secure_filename
import sys, traceback, os
from flask_login import current_user, login_user
from services.video_processor.models import User, UploadedVideo, Notification,ProcessedVideo
from services.video_processor.forms import LoginForm, RegistrationForm
from flask_login import login_required, logout_user
from flask import jsonify, flash, current_app, send_from_directory
import json
import services.video_processor.silence_remover as silence_remover
from time import time

ALLOWED_EXTENSIONS = set(['mp4','avi','h264','mov'])


def start_user_paths(username):
    """
    On login, ensure that all required server side user paths are created
    """
    paths = []
    paths.append(os.path.join(current_app.config["IMG_FOLDER"],username))
    paths.append(os.path.join(current_app.config["UPLOAD_FOLDER"],username))
    paths.append(os.path.join(current_app.config["PROCESSED_FOLDER"],username))
    paths.append(os.path.join(os.getcwd(),username))

    for p in paths:
        if not os.path.exists(p):
            os.makedirs(p)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@bp.route('/download_file')
def download_file():
    print "starting download"
    filename = request.args.get('filename')
    path = os.path.join(os.getcwd(), current_app.config['PROCESSED_FOLDER'])
    user_path = os.path.join(path,current_user.username)
    print "looking in: "+str(user_path)+" for file: "+str(filename)
    return send_from_directory(user_path,
                               filename, as_attachment=True)


@bp.route('/cancel_job')
def cancel_job():
    job_name = request.args.get('job_name')
    current_task = current_user.get_task_in_progress(job_name)
    if current_task:
        rq_job = current_task.get_rq_job()
        if rq_job:
            rq_job.set_status("stop")
            current_task.complete = True
            db.session.commit()
    return redirect(url_for('routes.dashboard'))

@bp.route('/remove_file')
def remove_file():
    pv_id = request.args.get('pv_id')
    pv = ProcessedVideo.query.get(int(pv_id))
    filename = pv.filename
    db.session.delete(pv)
    db.session.commit()
    path = os.path.join(os.getcwd(), current_app.config['PROCESSED_FOLDER'])
    user_path = os.path.join(path,current_user.username)
    os.system("rm "+str(os.path.join(user_path,filename)))
    return redirect(url_for('routes.dashboard'))


@bp.route('/dashboard', methods=['GET'])
@login_required
def dashboard():
    user_uploads = current_user.uploads.all()
    user_processed_videos = current_user.processed_videos.all()
    pending_videos = []
    analysed_videos = []
    for upload in user_uploads:
        if not upload.complete:
            pending_videos.append(upload)
            if upload.analysed:
                analysed_videos.append(upload)
    
    #change waveform name
    user_path = os.path.join(current_app.config["IMG_FOLDER"],current_user.username)
    anti_cache_name = str(int(time()))+"_waveform.png"
    for file in os.listdir(user_path):
        if file.endswith(".png"):
            os.system("mv "+os.path.join(user_path, file)+" "+os.path.join(user_path, anti_cache_name))

    waveform_image = os.path.join('static/img/'+str(current_user.username), anti_cache_name)
    return render_template('dashboard.html',uploads=pending_videos,processed_videos=user_processed_videos,analysed_videos=analysed_videos,waveform_image=waveform_image)


@bp.route('/')
def index():
    return redirect(url_for('routes.dashboard'))


@bp.route("/sendfile", methods=["POST"])
def send_file():
    print "in send file"
    fileob = request.files["file2upload"]
    filename = secure_filename(fileob.filename)
    if allowed_file(filename):
        upload_object = UploadedVideo(filename=filename, user_id=current_user.id)
        db.session.add(upload_object)
        db.session.commit()
        user_path = os.path.join(current_app.config["UPLOAD_FOLDER"],current_user.username)
        save_path = "{}/{}".format(user_path, filename)
        fileob.save(save_path)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    else:
        return json.dumps({'success':False}), 415, {'ContentType':'application/json'}


@bp.route("/filenames", methods=["GET"])
def get_filenames():
    user_path = os.path.join(current_app.config["UPLOAD_FOLDER"],current_user.username)
    filenames = os.listdir(user_path)
    modify_time_sort = lambda f: os.stat(user_path+"/{}".format(f)).st_atime
    filenames = sorted(filenames, key=modify_time_sort)
    return_dict = dict(filenames=filenames)
    return jsonify(return_dict)


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('routes.dashboard'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('routes.login'))
        login_user(user, remember=form.remember_me.data)
        start_user_paths(user.username)
        return redirect(url_for('routes.dashboard'))
    return render_template('login.html', title='Sign In', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('routes.login'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('routes.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        start_user_paths(user.username)
        return redirect(url_for('routes.login'))
    return render_template('register.html', title='Register', form=form)


@bp.route('/process_video',methods=['GET', 'POST'])
@login_required
def process_video():
    if current_user.get_task_in_progress('process_video'):
        flash(('Already processing video for this user'))
    else:
        current_user.launch_task('process_video', 'Starting video processing...')
        db.session.commit()
    return redirect(url_for('routes.dashboard'))


@bp.route('/analyse_video',methods=['GET', 'POST'])
@login_required
def analyse_video():
    if current_user.get_task_in_progress('analyse_video'):
        flash(('Already analysing video for this user'))
    else:
        user_uploads = current_user.uploads.all()
        last_upload = current_user.uploads.order_by(UploadedVideo.timestamp.desc())[0]
        last_upload.silence_length = request.json["silence_length"]
        last_upload.noise_threshold = request.json["noise_threshold"]
        db.session.commit()
        current_user.launch_task('analyse_video', 'Starting video analysis...')
        db.session.commit()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 


@bp.route('/cancel_upload')
@login_required
def cancel_upload():
    upload_id = request.args.get('upload_id')
    upload = UploadedVideo.query.get(int(upload_id))
    db.session.delete(upload)
    db.session.commit()
    return redirect(url_for('routes.dashboard'))


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.desc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
} for n in notifications])