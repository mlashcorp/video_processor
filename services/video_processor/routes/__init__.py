from flask import Blueprint

bp = Blueprint('routes', __name__)

from services.video_processor.routes import views