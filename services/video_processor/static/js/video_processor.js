$(function(){

    var fileUploadSuccess = function(data){
        console.log("upload was a success")
        /*var silence_length = document.getElementById("slider-time").value
        var noise_threshold = document.getElementById("slider-noise").value
        var req_sliders = {
            url: "/config",
            method: "post",
            processData: false,
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify({"silence_length":silence_length,
                    "noise_threshold": noise_threshold
                },null,'\t')
        };
        
        var promise = $.ajax(req_sliders);
        promise.then(configSuccess, configFail);*/
        window.location.href = "dashboard";
    };

    var fileUploadFail = function(data){
        console.log("upload failed");
        $("div#upload-message").replaceWith("<div id=upload-message>Illegal file type. <br> supported formats: avi, mp4, h264, mov </div>");
    };

    var analyseSuccess = function(data){
        console.log("analyse was a success");
        window.location.href = "dashboard";

    };

    var analyseFail = function(data){
        console.log("analyse failed")
    };


    var configSuccess = function(data){
        window.location.href = "dashboard";
    };

    var configFail = function(data){
        console.log("upload failed")
    };

    var dragHandler = function(evt){
        evt.preventDefault();
    };

    var dropHandler = function(evt){
        evt.preventDefault();
        $("div#upload-message").replaceWith("<div id=upload-message>Uploading...</div>")
        document.getElementById("up-progress").style.visibility= "visible" ;

        var files = evt.originalEvent.dataTransfer.files;
        var formData = new FormData();

        formData.append("file2upload", files[0]);
        
        var req = {
            url: "/sendfile",
            method: "post",
            processData: false,
            contentType: false,
            data: formData,
            xhr: function(){
                //Get XmlHttpRequest object
                 var xhr = $.ajaxSettings.xhr() ;
                //Set onprogress event handler
                 xhr.upload.onprogress = function(data){
                    var perc = Math.round((data.loaded / data.total) * 100);
                    $('#up-progress').text(perc + '%');
                    $('#up-progress').css('width',perc + '%');
                 };
                 return xhr ;
            }
        };
        var promise = $.ajax(req);
        promise.then(fileUploadSuccess, fileUploadFail);

    };

    var dropHandlerSet = {
        dragover: dragHandler,
        drop: dropHandler
    };


    function saveValue(e){
        console.log("saving id: "+e.id+" with value: "+e.value);
        var id = e.id;  // get the sender's id to save it . 
        var val = e.value; // get the value. 
        localStorage.setItem(id, val.toString());// Every time user writing something, the localStorage's value will override . 
    }

    $('.analyse-btn').click(function() { 
        console.log("clicked analyse button ...");
        var silence_length = document.getElementById("slider-time").value;
        var noise_threshold = document.getElementById("slider-noise").value;
        saveValue(document.getElementById("slider-time"));
        saveValue(document.getElementById("slider-noise"));
        
        var req = {
            url: '/analyse_video',
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify({"silence_length":silence_length,
            "noise_threshold": noise_threshold
            },null,'\t')
        };  
        var promise = $.ajax(req);
        promise.then(analyseSuccess, analyseFail);
    });
    

    $(".upload-drop-zone").on(dropHandlerSet);



});

